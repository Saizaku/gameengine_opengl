#ifndef MAINGAME_H
#define MAINGAME_H

#include "Bullet.h"

#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <vector>
#include <engine/inc/Sprite.h>
#include <engine/inc/GLSLProgram.h>
#include <engine/inc/GLTexture.h>
#include <engine/inc/Window.h>
#include <engine/inc/Camera2D.h>
#include <engine/inc/SpriteBatch.h>
#include <engine/inc/InputManager.h>
#include <engine/inc/Timing.h>

enum class GameState {PLAY, EXIT};

class MainGame
{
public:
  MainGame();
  ~MainGame();
  
  void run();
  
private:
  Engine::Window _window;
  int _screenWidth;
  int _screenHeight;
  GameState _gameState;
  Engine::GLSLProgram _colorProgram;
  Engine::Camera2D _camera;
  Engine::SpriteBatch _spriteBatch;
  Engine::InputManager _inputManager;
  Engine::FpsLimiter _fpsLimiter;
  
  std::vector<Bullet> _bullets;
  
  float _maxFPS;
  float _fps;
  float _time;
  
  void initSystems();
  void gameLoop();
  void processInput();
  void drawGame();
  void initShaders();
  
};

#endif
