#ifndef BULLET_H
#define BULLET_H

#include <glm/glm.hpp>
#include <engine/inc/SpriteBatch.h>
#include <string>

class Bullet{
public:
  Bullet(glm::vec2 pos, glm::vec2 dir, float speed, std::string texturePath, int lifeTime);
  ~Bullet();
  
  void draw(Engine::SpriteBatch& spriteBatch);
  //returns true when it should be destroyed
  bool update();
  
private:
  int _lifeTime;
  float _speed;
  glm::vec2 _direction;
  glm::vec2 _position;
  unsigned int _texture;
};

#endif
