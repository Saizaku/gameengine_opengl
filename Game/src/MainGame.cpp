#include "MainGame.h"

#include <engine/inc/Engine.h>
#include <engine/inc/Errors.h>
#include <engine/inc/ResourceManager.h>

#include <iostream>
#include <string>

MainGame::MainGame():
_screenWidth(1024), 
_screenHeight(768),
_time(0),
_gameState(GameState::PLAY),
_maxFPS(120.f){
_camera.init(_screenWidth, _screenHeight);
}

MainGame::~MainGame(){
    
}
  
void MainGame::run(){
  initSystems();
  
  
  //_playerTexture = ImageLoader::loadPNG("textures/jimmyJump_pack/PNG/CharacterRight_Standing.png");
  
  gameLoop();
}
  
void MainGame::initSystems(){
  Engine::init();
  
  _window.create("Game Engine", _screenWidth, _screenHeight, 0);
  
  initShaders();
  
  _spriteBatch.init();
  _fpsLimiter.init(_maxFPS);
}

void MainGame::initShaders(){
  _colorProgram.compileShaders("engine/shaders/colorShading.vert", "engine/shaders/colorShading.frag");
  _colorProgram.addAttribute("vertexPosition");
  _colorProgram.addAttribute("vertexColor");
  _colorProgram.addAttribute("vertexUV");
  _colorProgram.linkShaders();
}

void MainGame::gameLoop(){
  while(_gameState != GameState::EXIT){
    _fpsLimiter.begin();
    
    processInput();
    _time+= 0.01;
    
    _camera.update();
    
    for(int i = 0; i < _bullets.size();){
      if(_bullets[i].update() == true){
        _bullets[i] = _bullets.back();
        _bullets.pop_back();
      } else{
        i++;
      }
    }
    
    drawGame();
    
    _fps = _fpsLimiter.end();
    
    //print only once every 10 frames
    static int frameCounter = 0;
    frameCounter++;
    if(frameCounter == 10000){
      std::cout << _fps << std::endl;
      frameCounter = 0;
    }
    
  }
}

void MainGame::processInput(){
  SDL_Event evnt;
  
  const float CAMERA_SPEED = 1.0f;
  const float SCALE_SPEED = 0.01f;
  
  while(SDL_PollEvent(&evnt)){
    switch(evnt.type){
      case SDL_QUIT:
        _gameState = GameState::EXIT;
        break;
      case SDL_MOUSEMOTION:
        _inputManager.setMouseCoords(evnt.motion.x, evnt.motion.y);
        break;
      case SDL_KEYDOWN:
        _inputManager.pressKey(evnt.key.keysym.sym);
        break;
      case SDL_KEYUP:
        _inputManager.releaseKey(evnt.key.keysym.sym);
        break;
      case SDL_MOUSEBUTTONDOWN:
        _inputManager.pressKey(evnt.button.button);
        break;
      case SDL_MOUSEBUTTONUP:
        _inputManager.releaseKey(evnt.button.button);
        break;
    }
  }
  
  if(_inputManager.isKeyPressed(SDLK_w)){
    _camera.setPosition(_camera.getPosition() + glm::vec2(0.0,CAMERA_SPEED));
  }
  if(_inputManager.isKeyPressed(SDLK_s)){
    _camera.setPosition(_camera.getPosition() + glm::vec2(0.0,-CAMERA_SPEED));
  }
  if(_inputManager.isKeyPressed(SDLK_a)){
    _camera.setPosition(_camera.getPosition() + glm::vec2(-CAMERA_SPEED,0.0));
  }
  if(_inputManager.isKeyPressed(SDLK_d)){
    _camera.setPosition(_camera.getPosition() + glm::vec2(CAMERA_SPEED,0.0));
  }
  if(_inputManager.isKeyPressed(SDLK_q)){
    _camera.setScale(_camera.getScale() + SCALE_SPEED);
  }
  if(_inputManager.isKeyPressed(SDLK_e)){
    _camera.setScale(_camera.getScale() - SCALE_SPEED);
  }
  if(_inputManager.isKeyPressed(SDL_BUTTON_LEFT)){
    glm::vec2 mouseCoords = _inputManager.getMouseCoords();
    mouseCoords = _camera.convertScreenToWorld(mouseCoords);
    
    glm::vec2 playerPosition(0.0f);
    glm::vec2 direction = mouseCoords - playerPosition;
    direction = glm::normalize(direction);
    
    _bullets.emplace_back(playerPosition, direction, 5.f, "engine/textures/jimmyJump_pack/PNG/Bubble_Small.png", 1000);
    
  }
}

void MainGame::drawGame(){
  
  glClearDepth(1.0);
  //Clear the color and depth buffer
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  
  _colorProgram.use();
  
  glActiveTexture(GL_TEXTURE0);
  
  //Set the texture
  GLint textureLocation = _colorProgram.getUniformLocation("mySampler");
  glUniform1i(textureLocation, 0);
  
  //Set the camera matrix
  GLint pLocation = _colorProgram.getUniformLocation("P");
  glm::mat4 cameraMatrix = _camera.getCameraMatrix();
  glUniformMatrix4fv(pLocation, 1, GL_FALSE, &(cameraMatrix[0][0]));
  
  //GLint timeLocation = _colorProgram.getUniformLocation("time");
  //glUniform1f(timeLocation, _time);
  
  _spriteBatch.begin();
  
  for(int i = 0; i < _bullets.size(); i++){
    _bullets[i].draw(_spriteBatch);
  }
  
  glm::vec4 pos(0.0f,0.0f,50.0f,50.0f);  
  glm::vec4 uv(0.0f, 0.0f, 1.0f, 1.0f);
  Engine::GLTexture texture = Engine::ResourceManager::getTexture("engine/textures/jimmyJump_pack/PNG/CharacterRight_Standing.png");
  Engine::Color color;
  color.r = 255;
  color.g = 255;
  color.b = 255;
  color.a = 255;
  
  _spriteBatch.draw(pos, uv, texture.id, 0.f, color);
  _spriteBatch.end();
  
  _spriteBatch.renderBatch();
  
  //glBindTexture(GL_TEXTURE_2D, 0);
  _colorProgram.unuse();
  
  _window.swapBuffer();
}
