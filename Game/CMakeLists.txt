cmake_minimum_required(VERSION 3.7)

project(game)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/engine/cmake/modules/")

set (CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR})

set (CMAKE_EXPORT_COMPILE_COMMANDS 1)

add_subdirectory(engine)
add_subdirectory(src)
