## Compilation
To compile this code open a terminal in the root folder and run:
- `$cmake .`
- `$make`
## Dependencies
- OpenGL
- SDL2
- Glew
- CMake
- G++/GCC
- SOIL