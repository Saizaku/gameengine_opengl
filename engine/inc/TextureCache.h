#ifndef TEXTURECACHE_H
#define TEXTURECACHE_H

#include "GLTexture.h"

#include <map>
#include <string>

namespace Engine{ 
class TextureCache{
public:
  TextureCache();
  ~TextureCache();
  
  GLTexture getTexture(std::string texturePath);
  
private:
  std::map<std::string, GLTexture> _textureMap;
};
}
#endif
