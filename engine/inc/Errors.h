#ifndef ERRORS_H
#define ERRORS_h

#include <string>
namespace Engine{ 

extern void fatalError(std::string errorString);

}
#endif
