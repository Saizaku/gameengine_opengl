#ifndef TIMING_H
#define TIMING_H

namespace Engine{
class FpsLimiter{
public:
  FpsLimiter();
  void init(float maxFPS);
  
  void setMaxFPS(float maxFPSFPS);
  
  void begin();
  
  //end returns the current fps
  float end();
  
private:
  void calculateFPS();
  
  float _fps;
  float _maxFPS;
  float _frameTime;
  unsigned int _startTicks;
};
}

#endif
