#include "ImageLoader.h"
#include <SOIL/SOIL.h>
#include "Errors.h"
#include <cstdio>
namespace Engine{ 
GLTexture ImageLoader::loadPNG(std::string filePath){
    GLTexture texture = {};
    int width;
    int height;

    //Load the texture using soil
    texture.id = SOIL_load_OGL_texture(
                                       filePath.c_str(),
                                       SOIL_LOAD_AUTO,
                                       SOIL_CREATE_NEW_ID,
                                       SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT
    );
    
    printf("Loaded texture with id: %u", texture.id);  
    
    if(texture.id == 0){
        printf("%s", SOIL_last_result());
        fatalError("\n SOIL load failed.");
    }

    //Bind the texture
    glBindTexture(GL_TEXTURE_2D, texture.id);

    //Get the image dimensions and image data
    glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &width);
    glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &height);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

    glGenerateMipmap(GL_TEXTURE_2D);

    glBindTexture(GL_TEXTURE_2D, 0);

    texture.width = width;
    texture.height = height;
    return texture;
}
}
