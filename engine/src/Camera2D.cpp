#include "Camera2D.h"

namespace Engine{

Camera2D::Camera2D():
_needsMatrixUpdate(true),
_position(0.0f,0.0f),
_cameraMatrix(1.0f),
_orthoMatrix(1.0f),
_scale(1.0f),
_screenWidth(1024),
_screenHeight(768){
  
}

Camera2D::~Camera2D(){
  
}

void Camera2D::init(int screenWidth, int screenHeight){
  _screenWidth = screenWidth;
  _screenHeight = screenHeight;
  
  _orthoMatrix = glm::ortho(0.0f, (float)_screenWidth, 0.0f, (float)_screenHeight);
}

void Camera2D::update(){
  //Only update if our position or scale have changed         
  if (_needsMatrixUpdate) {                          
    //Camera Translation             
    glm::vec3 translate(-_position.x+_screenWidth/2, -_position.y + _screenHeight/2, 0.0f);             
    _cameraMatrix = glm::translate(_orthoMatrix, translate);  
    
    //Camera Scale             
    glm::vec3 scale(_scale, _scale, 0.0f);             
    _cameraMatrix = glm::scale(glm::mat4(1.0f), scale) * _cameraMatrix;       
    
    _needsMatrixUpdate = false;         
  }
}

glm::vec2 Camera2D::convertScreenToWorld(glm::vec2 screenCoords){
  //Make 0 the center
  screenCoords -= glm::vec2(_screenWidth/2, _screenHeight/2);
  screenCoords *= glm::vec2(1.f, -1.f);
  //Scale the coords
  screenCoords /= _scale;
  //Translate with the camera translation
  screenCoords += _position;
  
  return screenCoords;
}

}
