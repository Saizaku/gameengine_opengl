#include <SDL2/SDL.h>
#include <GL/glew.h>

#include "Engine.h"

namespace Engine{ 
int init(){
  //Initialize SDL
  SDL_Init(SDL_INIT_EVERYTHING);
  
  //Enable double buffering in OpenGL
  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
  
  return 0;
}
}
