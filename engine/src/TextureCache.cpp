#include "TextureCache.h" 
#include "ImageLoader.h"
#include <iostream>
namespace Engine{ 
TextureCache::TextureCache(){
  
}

TextureCache::~TextureCache(){
  
}

GLTexture TextureCache::getTexture(std::string texturePath){
  //lookup the texture and see if its in the map
  auto mit = _textureMap.find(texturePath);
  std::cout << texturePath << std::endl;
  //check if its not in the _textureMap
  if(mit == _textureMap.end()){
    //load te texture
    GLTexture newTexture = ImageLoader::loadPNG(texturePath);
    
    //insert it into the map
    _textureMap.insert(make_pair(texturePath, newTexture));
    
    std::cout << "Loaded Texture \n";
    return newTexture;
  }
  std::cout << "Used Cached Texture \n";
  return mit->second;
}
}
