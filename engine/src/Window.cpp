#include "Window.h" 
#include "Errors.h"

namespace Engine{

Window::Window(){
  
}

Window::~Window(){
  
}

int Window::create(std::string windowName, int screenWidth, int screenHeight, unsigned int currentFlags){
  Uint32 flags = SDL_WINDOW_OPENGL;
  
  if(currentFlags & INVISIBLE){
    flags |= SDL_WINDOW_HIDDEN;
  }
  
  if(currentFlags & FULLSCREEN){
    flags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
  }
  
  if(currentFlags & BORDERLESS){
    flags |= SDL_WINDOW_BORDERLESS;
  }
  
  //Create the window
  _sdlWindow = SDL_CreateWindow(windowName.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                             screenWidth, screenHeight, flags);
  if(_sdlWindow == nullptr){
    fatalError("SDL Window could not be created!");
  }
  
  //Setup OpenGl context
  SDL_GLContext glContext = SDL_GL_CreateContext(_sdlWindow);
  if(glContext == nullptr){
    fatalError("SDL_GL context could not be created!");
  }
  
  //compatiblity layer
  GLenum error = glewInit();
  if(error != GLEW_OK){
    fatalError("Could not Initialize glew!");
  }
  
  //Check the OpenGL version
  std::printf("*** OpenGL Version: %s *** \n", glGetString(GL_VERSION));
  
  glClearColor(0.f,0.f,1.f,1.f);
  
  //Disable VSYNC
  SDL_GL_SetSwapInterval(0);
  
  //Enable alpha blending
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  
  return 0;
}

void Window::swapBuffer(){
  //Swap the buffer and draw to the screen
  SDL_GL_SwapWindow(_sdlWindow);
}
}
